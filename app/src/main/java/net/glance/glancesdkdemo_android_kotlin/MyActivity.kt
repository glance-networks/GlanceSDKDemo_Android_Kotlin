package net.glance.glancesdkdemo_android_kotlin

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.TextView
import net.glance.android.*
import java.util.HashMap

// Visitor demo implementation is below. NOTE:
// before reading through this code, make sure you've read through the README

// Global variable
var sessionKey : String? = null
val GLANCE_GROUP_ID = 15687
val VISITOR_ID : String? = null // "123four"

// make sure your class implements VisitorListener
class MyActivity : AppCompatActivity(), VisitorListener {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_my)

        // Initialize the Visitor with the group ID provided by Glance
        if (VISITOR_ID != null) {
            Visitor.init(this, GLANCE_GROUP_ID, "", "", "", "", VISITOR_ID)
        } else {
            Visitor.init(this, GLANCE_GROUP_ID, "", "", "", "", "")
        }
        // Mask the session key
        Visitor.addMaskedViewId(R.id.textView2)
        // Make sure the Visitor is listening for events on this Activity
        Visitor.addListener(this)

        // Uncomment these lines to use your own custom UI, including custom agent video viewer
//        Visitor.defaultUI(false) // disable default UI
//        Visitor.setCustomSessionViewId(R.id.glance_agent_viewer)

        val startSessionBtn = findViewById<Button>(R.id.startSession)
        startSessionBtn.setOnClickListener {
            startSession()
        }
        val endSessionBtn = findViewById<Button>(R.id.endSession)
        endSessionBtn.setOnClickListener {
            endSession()
        }
        val openBrowserBtn = findViewById<Button>(R.id.openBrowser)
        openBrowserBtn.setOnClickListener {
            openBrowser()
        }
    }

    override fun onResume() {
        super.onResume()

        updateView()
    }

    override fun onDestroy() {
        super.onDestroy()

        Visitor.removeMaskedViewId(R.id.textView2)

        // Make sure to remove the listener before this Activity is destroyed
        Visitor.removeListener(this)
    }

    // this is called on the Glance Event thread
    override fun onGlanceVisitorEvent(event: Event) {
        if (event.code == EventCode.EventConnectedToSession) {
            // Show any UI to indicate the session has started
            // If not using a known session key, you can get the random key here (sessionKey)
            // to display to the user to read to the agent
            sessionKey = event.GetValue("sessionkey")
            updateView()
        } else if (event.code == EventCode.EventSessionEnded) {
            sessionKey = null
            updateView()
        } else if (event.type == EventType.EventWarning ||
                event.type == EventType.EventError ||
                event.type == EventType.EventAssertFail) {
            // Best practice is to log code and message of all events of these types
            Log.d("EVENT_TYPE", event.type.toString() + " " + event.messageString)
        }
        if (VISITOR_ID != null) {
            when (event.code) {
                EventCode.EventVisitorInitialized -> {
                    PresenceVisitor.connect()
                }
                EventCode.EventPresenceConnected -> {
                    presenceConnected()
                }
                EventCode.EventPresenceConnectFail -> {
                    Log.d("EVENT_TYPE", "Presence connection failed (will retry): " + event.messageString)
                }
                EventCode.EventPresenceShowTerms -> {
                    // You only need to handle this event if you are providing a custom UI for terms and/or confirmation
                    Log.d("EVENT_TYPE", "Agent signalled ShowTerms")
                }
                EventCode.EventPresenceBlur -> {
                    // This event notifies the app that the visitor is now using another app or website
                }
            }
        }
    }

    private fun updateView() {
        runOnUiThread {
            var startSessionButton = findViewById<Button> (R.id.startSession);
            var endSessionButton = findViewById<Button> (R.id.endSession);
            if (sessionKey != null) {
                startSessionButton.setVisibility(View.GONE);
                endSessionButton.setVisibility(View.VISIBLE);
                endSessionButton.isEnabled = true
                findViewById<TextView> (R.id.textView2).setText(sessionKey);
            } else {
                startSessionButton.setVisibility(View.VISIBLE);
                startSessionButton.isEnabled = true
                endSessionButton.setVisibility(View.GONE);
                findViewById<TextView> (R.id.textView2).setText("Click button to below to start session");
            }
        }
    }

    private fun presenceConnected() {
        runOnUiThread {
            findViewById<TextView> (R.id.presenceIndicator).setText("Presence Connected")
            findViewById<Button> (R.id.startSession).setVisibility(View.GONE)
        }
    }

    fun startSession() {
        val button = findViewById<Button>(R.id.startSession)
        button.isEnabled = false
        Visitor.startSession()
    }

    fun endSession() {
        val button = findViewById<Button>(R.id.endSession)
        button.isEnabled = false
        Visitor.endSession()
    }

    fun openBrowser() {
        val context = this
        val intent = Intent(context, WebView::class.java)
        startActivity(intent)

        if (VISITOR_ID != null) {
            val url = HashMap<String, String>()
            url["url"] = "webview https://www.glance.net"
            PresenceVisitor.presence(url)
        }
    }
}
